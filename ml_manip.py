"""
this file is used to manipulate machine learning models used in pate_framework.py
"""
import numpy as np
import logging
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Flatten
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.losses import categorical_crossentropy
import re


class ModelManipulator:

    def __init__(self, pate, data_manipulator, **kwargs):
        
        self.pate = pate
        self.data_manipulator = data_manipulator
        self.pate_dict_teacher = pate.dict_teacher
        self.pate_dict_student = pate.dict_student

    def create_pate_models(self):
        for teacher_id in self.pate_dict_teacher.keys():
            teacher_model = self.define_model("Teacher", teacher_id)
            self.pate_dict_teacher[teacher_id].append(teacher_model)

        for student_id in self.pate_dict_student.keys():
            student_model = self.define_model("Student", student_id)
            self.pate_dict_student[student_id].append(student_model)
                    
    def define_model(self, model_type, model_id):
        
        if self.pate.aut_cifar is True:
            logging.info("Building {} model for {} ".format(model_type, model_id))
            if re.search("^teacher*", model_id.lower()):
                model = Sequential(name=model_id)
                model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same',
                                 input_shape=(32, 32, 3)))
                model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
                model.add(MaxPooling2D((2, 2)))
                model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
                model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
                model.add(MaxPooling2D((2, 2)))
                model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
                model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
                model.add(MaxPooling2D((2, 2)))
                model.add(Flatten())
                model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
                model.add(Dense(10, activation='softmax'))

                # compile model
                opt = SGD(learning_rate=0.001, momentum=0.9)
                model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

                return model

            elif re.search("^student*", model_id.lower()):
                model = Sequential(name=model_id)
                model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same',
                                 input_shape=(32, 32, 3)))
                model.add(MaxPooling2D((2, 2)))
                model.add(Flatten())
                model.add(Dense(32, activation='relu', kernel_initializer='he_uniform'))
                model.add(Dense(10, activation='softmax'))

                # compile model
                opt = SGD(learning_rate=0.001, momentum=0.9)
                model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

                print(model.summary())
                logging.debug("model {} has been created".format(model.name))

                return model

        else:
            # if the model is for a teacher
            if re.search("^teacher*", model_id.lower()):

                model = Sequential(name=model_id)
                model.add(Dense(128, input_shape=((len(self.columns) - 1),)))
                model.add(Dense(64))
                model.add(Dense(32))
                model.add(Dense(len(self.label[0])))

                model.compile(optimizer=SGD(), loss=tf.keras.losses.BinaryCrossentropy(), metrics=['accuracy'])

                print(model.summary())
                logging.debug("model {} has been created".format(model.name))

                return model

            elif re.search("^student*", model_id.lower()):

                # if the model is for a student
                model = Sequential(name=model_id)
                model.add(Dense(32, input_shape=((len(self.columns) - 1),)))
                model.add(Dense(len(self.label[0])))

                model.compile(optimizer=SGD(), loss=tf.keras.losses.BinaryCrossentropy(), metrics=['accuracy'])

                print(model.summary())
                logging.debug("model {} has been created".format(model.name))

                return model

    @staticmethod
    def take_gradient_step(model, query, label):
        with tf.GradientTape() as tape:
            query_np = np.expand_dims(query, axis=0)
            teacher_label_np = np.expand_dims(label, axis=0)
            prediction = model(query_np)
            loss = categorical_crossentropy(teacher_label_np, prediction)

        gradients = tape.gradient(loss, model.trainable_variables)
        model.optimizer.apply_gradients(zip(gradients,
                                        model.trainable_variables))
        
        return 1
    
    def log_teacher_models_summaries(self):
        for teacher_id in self.pate_dict_teacher.keys():
            logging.info("teacher model summary for {}".format(teacher_id))
            logging.info(self.pate_dict_teacher[teacher_id][0].summary())
            
    def log_student_models_summaries(self):
        for student_id in self.pate_dict_student.keys():
            logging.info("student model summary for {}".format(student_id))
            logging.info(self.pate_dict_student[student_id][0].summary())
