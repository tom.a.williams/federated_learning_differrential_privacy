import logging

import numpy as np


class DpEngine:

    def __init__(self,
                 sensitivity=1,
                 target_epsilon=0.1,
                 target_delta=1e-5,
                 noise_count=1,
                 noise_generation_mechanism="original_lap",
                 threshold=2,
                 **kwargs):

        """
        :param sensitivity: sensitivity of the function, can be either L1-norm(manhattan distance) or L2-norm(euclidean
        distance) depending on the noise_generation_mechanism being used.
        :param target_epsilon: the privacy budget.
        :param target_delta: probability of the privacy guarantee not holding, i.e Pr[(A(D)/A(D')) > epsilon] <= delta.
        the target delta should be chosen equal or less than 1/N, where N is the size of the dataset.
        :param noise count: number of noise values to generate (single value or an array).
        :param approach: the noise_generation_mechanism to use for noise generation.
        """
        assert (
                sensitivity >= 0
        ), "Sensitivity cannot be a negative number"
        self.sensitivity = sensitivity
        logging.info(f"Sensitivity value is: {self.sensitivity}")

        assert (
                target_epsilon > 0 and isinstance(target_epsilon, float)
        ), "Epsilon must be a float and strictly positive"
        self.target_epsilon = target_epsilon
        logging.info(f"Privacy budget epsilon: {self.target_epsilon}")

        assert (
            target_delta > 0 and isinstance(target_delta, float)
        ), "Delta must be a float and strictly positive"
        self.target_delta = target_delta
        logging.info(f"Delta value:{self.target_delta}")

        assert (
                noise_count > 0 and isinstance(noise_count, int)
        ), "Noise count must a positive integer"
        self.noise_count = noise_count
        logging.info(f"Number of noise value")

        known_mechanisms = ["original_lap", "original_gauss", "dl_dp_gauss", "optimal_gauss"]
        if not isinstance(noise_generation_mechanism, str):
            raise AttributeError(f"The noise_generation_mechanism parameter cannot be of type "
                                 f"{type(noise_generation_mechanism)} "
                                 f"Please provide a string")
        elif noise_generation_mechanism not in known_mechanisms:
            raise Exception(f"Unknown value for parameter noise_generation_mechanism: {noise_generation_mechanism}"
                            f"Available mechanisms: {known_mechanisms}")
        else:
            self.mechanism = noise_generation_mechanism
            logging.info(f"Noise generation noise_generation_mechanism chosen: {self.mechanism}")

        assert (
            threshold > 0
        ), "Threshold cannot be negative"
        self.threshold = threshold
        logging.info(f"Threshold chosen: {self.threshold}")

    def generate_noise(self):
        """
        Generate random noise
        """
        if self.mechanism == "original_lap":
            """
            Using the formula introduced in "the algorithmic foundations of differential privacy".
            """
            scale = self.sensitivity / self.target_epsilon  # the higher the scale the flatter the distribution graph
            location = 0  # i.e the mean

            return np.random.laplace(location, scale, self.noise_count)

        elif self.mechanism == "original_gauss":
            """
            Using the original formula introduced in the "the algorithmic foundations of differential privacy".
            """
            mean = 0
            std = np.sqrt(self.sensitivity * np.log(1 / self.target_delta) / self.target_epsilon)

            return np.random.normal(mean, std, self.noise_count)

        elif self.mechanism == "dl_dp_gauss":
            """
            Using the formula introduced in the paper "Deep learning with differential privacy".
            This noise generated is sub-optimal when epsilon --> 0.
            """
            mean = 0
            std = self.sensitivity * np.sqrt(2 * np.log(1.5 / self.target_delta)) / self.target_epsilon

            return np.random.normal(mean, std, self.noise_count)

        elif self.mechanism == "optimal_gauss":
            # TODO
            """
            Using the formula introduced in the paper "Improving the Gaussian Mechanism for Differential Privacy:
            Analytical Calibration and Optimal Denoising".
            This is an optimization problem where the optimal value of sigma is
            """
            pass

    def original_pate_aggregator(self, teachers_votes_vector):
        """
          - The original noisy max algorithm of PATE introduced in the paper SEMI-SUPERVISED KNOWLEDGE TRANSFER
            FOR DEEP LEARNING FROM PRIVATE TRAINING DATA
          """
        # add laplacian noise to teachers votes
        noisy_votes = [each_vote + self.generate_noise() for each_vote in teachers_votes_vector]

        return noisy_votes.index(max(noisy_votes))
        # return the index of the maximum value of the
        # noisy votes along with its index

    def confident_GNMAX_aggregator(self, threshold, teachers_votes_vector):
        """
        - This algorithm was introduced in the paper "Scalable private learning with PATE"
        """
        # test for teachers consensus, if it exceeds a specified threshold
        # then proceed with the noisy-max noise_generation_mechanism
        if (np.max(teachers_votes_vector) + self.generate_noise()) > threshold:

            noisy_votes = [each_vote + self.generate_noise() for each_vote in
                           teachers_votes_vector]  # the usual noisy max noise_generation_mechanism

            return max(noisy_votes), noisy_votes.index(max(noisy_votes))

        else:

            return 0  # no answer is given to the query
